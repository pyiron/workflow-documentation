# workflow-documentation
## Purpose of the repository
As a central repository, here the workflows are documented. A guide for development of pyiron workflows can be also added here.

## Pyiron workflows
pyiron workflows consist of jupyter notebook or more jupyter notebooks plus the conda `environment.yml` file defining the strict dependencies.
In addition, they can contain further files like python classes or parameter files. 
They are published in [Github repositories](https://github.com/materialdigital) as tagged releases, and it is recommended to use Github actions to continuously test the workflows.  

## Testing the workflows  
To test the workflows, for each repository a mybinder link is presented (**find in the table below**). Mybinder provides a computational environment for jupyter notebooks with pre-defined conda environments for each specific workflow repository. Therefore, it serves a great environment for testing the workflow notebooks, however, the data created and modified on mybinder is not persisted, therefore users should note that.

## Current pyiron workflows 

| Name | Repository | Binder Link | Description | Version |
|------|------------|-------------| ---------- | ------- |
| pyiron-workflow-thermalexpansion | https://github.com/materialdigital/pyiron-workflow-thermalexpansion | [![Binder](https://mybinder.org/badge_logo.svg)](https://binderdemo.mpcdf.mpg.de/v2/glMPCDF/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fpyiron-workflow-thermalexpansion/HEAD) | Calculate the thermal expansion for different interatomic potentials using LAMMPS | 0.0.2 |
| pyiron-workflow-damask | https://github.com/materialdigital/pyiron-workflow-damask | [![Binder](https://mybinder.org/badge_logo.svg)](https://binderdemo.mpcdf.mpg.de/v2/glMPCDF/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fpyiron-workflow-damask/HEAD) | Calculate temperature dependent elastic constants using LAMMPS and then use those elastic constants in DAMASK to calculate the stress-strain curve of a polycrystal | 0.0.1 |
| pyiron-workflow-phasediagram | https://github.com/materialdigital/pyiron-workflow-phasediagram | [![Binder](https://mybinder.org/badge_logo.svg)](https://binderdemo.mpcdf.mpg.de/v2/glMPCDF/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fpyiron-workflow-phasediagram/HEAD) | Calculate the temperature and concentration dependent free energy with the quasiharmonic approximation using the sqsgenerator, LAMMPS and Phonopy. | 0.0.1 |
| pyiron-workflow-TEMImageSegmentation | https://github.com/materialdigital/pyiron-workflow-TEMImageSegmentation | [![Binder](https://mybinder.org/badge_logo.svg)](https://binderdemo.mpcdf.mpg.de/v2/glMPCDF/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fpyiron-workflow-TEMImageSegmentation/HEAD) | Analyzes a TEM image and perform a segmentation on it | 0.0.1 |
